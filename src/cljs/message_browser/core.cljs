(ns message-browser.core
  (:require [reagent.core :as reagent]
            [cljsjs.react]
            [cljs-http.client :as http]
            [chord.client :refer [ws-ch]]
            [cljs.core.async :refer [chan <! >! put! close! timeout]]
            [re-com.core :refer [gap line border v-box h-box box v-split scroller h-split title flex-child-style button input-textarea]]
            [cljs-uuid-utils.core :as uuid])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(enable-console-print!)

(defonce state (reagent/atom {:returns {}
                          :websocket nil}))

(defn log [m]
      (.log js/console m))

(defn toJSON [o]
      (let [o (if (map? o) (clj->js o) o)]
           (.stringify (.-JSON js/window) o nil 2)))

(defn receive-returns! [state server-ch reconnect!]
      (go-loop []
               (let [{:keys [message error] :as msg} (<! server-ch)]
                    (cond
                      error (println error)
                      (nil? message) (do
                                       (log "Websocket Connection Closed")
                                       (reconnect!))
                      message (let [uuid (uuid/uuid-string (uuid/make-random-uuid))
                                    message (assoc message :uuid uuid)]
                                   (println message)
                                   (swap! state update-in [:returns] assoc uuid message)))
                    (when message
                          (recur)))))

(defn connect-to-updates! []
      (go-loop []
        (let [{:keys [ws-channel error]} (<! (ws-ch "ws://localhost:8080/updates" {:format :json-kw}))]
             (if error
               (do
                 (log "ERROR Connecting")
                 (<! (timeout 1000))
                 (log "Retrying to connect again")
                 (recur))
               (do
                 (receive-returns! state ws-channel connect-to-updates!)
                 (swap! state assoc :ws ws-channel))))))

(defn disconnect-updates! []
      (when-let [ws (:websocket @state)]
                (log "Closing WS")
                (close! ws)
                (swap! state dissoc :ws)))


(def rounded-panel (merge (flex-child-style "1")
                          {:background-color "#fff"
                           :border           "1px solid lightgray"
                           :border-radius    "4px"
                           :padding          "0px 0px 0px 0px"}))

(defn selected-msg [msg]
      (if (= msg (:selected-msg @state))
        {:background "#D1E6EF"}))

(defn unread? [msg]
      (not (:msg-read msg)))

(defn message-item
      [msg]
      [:div {:style (merge {:padding "5px"}
                           (selected-msg msg))
             :on-click (fn []
                           (swap! state (fn [s]
                                            (let [msg (assoc msg :msg-read true)]
                                                 (-> s
                                                     (assoc-in [:returns (:uuid msg)]  msg)
                                                     (assoc :selected-msg msg))))))}
       (when (unread? msg)
             [:span.glyphicon.glyphicon-exclamation-sign {:style {:float "right" :font-size "16px" :color "#C72318"}} ""])
       [:span {:style {:font-size "16px" :font-weight "bold"}} (:label msg)]
       [:span {:style {:color "#777"}} (str " " (:timestamp msg))]])

(defn message-list
  [messages]
  (apply concat
    (for [msg messages]
      [[message-item msg]
       [line]])))

(defn message-list-panel
      []
      [v-box
       :width "100%"
       :children [[box
                   :align :center
                   :child [:h4 (str "Messages (" (count (:returns @state)) ")") ]]
                  [scroller
                   :h-scroll :off
                   :style rounded-panel
                   :child [v-box
                           :children (message-list (vals (:returns @state)))]]]])

(defn highlight-code [html-node]
      (let [nodes (.querySelectorAll html-node "pre code")]
           (loop [i (.-length nodes)]
                 (when-not (neg? i)
                           (when-let [item (.item nodes i)]
                                     (.highlightBlock js/hljs item))
                           (recur (dec i))))))

(def engraved-text
  {:text-shadow "0px 1px 1px #eee"
   :color "#888"
   :font-size "40px"})

(defn messsage-not-selected []
      [box
       :size "auto"
       :justify :center
       :align :center
       :style (merge rounded-panel
                     {:background "#ddd"})
       :child [:span
               {:style engraved-text}
               "No Message Selected"]])

(defn message-content-panel
      []
      (reagent/create-class
          {:component-did-update
           (fn [this]
               (let [node (reagent/dom-node this)]
                    (highlight-code node)))
           :reagent-render
           (fn []
               [scroller
                :h-scroll :off
                :child (if (:selected-msg @state)
                         [box
                          :size "auto"
                          :style rounded-panel
                          :child [:pre {:style {:width "100%"}}
                                  [:code.JSON.hljs
                                   (toJSON (:content (:selected-msg @state)))]]]
                         ;; else
                         [messsage-not-selected])])}))

(defn message-panel
      []
      (reagent/create-class
        {:component-did-mount connect-to-updates!
         :component-will-unmount disconnect-updates!
         :reagent-render
         (fn []
         [h-split
          :style {:padding "5px"}
          :panel-1 [message-list-panel]
          :panel-2 [message-content-panel]
          :initial-split 25
          :margin "0px"
          :splitter-size "5px"
          :size "100%"])}))

(defn main-page
      []
      [v-box
       :height "100%"
       :children [[message-panel]]])

(defn mount-root
      []
      (reagent/render [main-page] (.getElementById js/document "app")))

(defn init!
  []
  (mount-root))
